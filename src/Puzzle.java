public class Puzzle {
   /*
   Kasutatud materjal:
   https://gist.github.com/vo/2481737
    */
   public static int counter=0;
   public static void main (String[] args){
      for (String s : args){
         if(s.length() > 18)
            throw new RuntimeException("Peab olema väiksem kui 18 tähte: " +s);
         for (char c : s.toCharArray()){
            if(!Character.isLetter(c))
               throw new RuntimeException("Sõnad peavad koosnema ainult tähtedest: " +s);
         }
      }

      if(args.length != 3)
         throw new RuntimeException("Peab olema kolm parameetrit");
      System.out.println(args[0]+" + "+args[1]+" = "+ args[2]);
      String s6nad = args[0]+" + "+args[1]+" = "+ args[2];
      System.out.println(solve(s6nad));
      if (counter==0){
         System.out.println("Lahendusi pole");
      }
      else   System.out.println("lahendusi on: "+counter);
      counter=0;
   }




   static int eval(String q) {
      int val = 0;
      java.util.StringTokenizer st = new java.util.StringTokenizer(q, "*/+-", true);
      while (st.hasMoreTokens()) {
         String next = st.nextToken().trim();
         if (next.equals("+")) {
            val += Integer.parseInt(st.nextToken().trim());
         } else {
            val = Integer.parseInt(next);
         }
      }
      return val;
   }
      static String solve(String q) {
         char c = 0;
         for (int i = 0; i < q.length(); ++i) {
            if (Character.isAlphabetic(q.charAt(i))) {
               c = q.charAt(i);
               break;
            }
         }
         if (c == 0) {
            String[] ops = q.split("=");
            int o1 = eval(ops[0]), o2 = eval(ops[1]);
            if (o1 == o2){
               if ((q.matches("(.*)\\s0(.*)")) || q.matches("(.*)^0+(?!$)(.*)") ){

               }
               else {

                  System.out.println(q);
                  counter = counter + 1;

               }
            }


            else return "";
         } else {
            char[] dset = new char[10];
            for (int i = 0; i < q.length(); ++i)
               if (Character.isDigit(q.charAt(i)))
                  dset[q.charAt(i)-'0'] = 1;
            for (int i = 0; i < 10; ++i) {
               if (dset[i] == 0) {
                  String r = solve(q.replaceAll(String.valueOf(c),
                          String.valueOf(i)));
               if (!r.isEmpty())
                     return r;
               }
            }
         }
         return "";
      }

   }


